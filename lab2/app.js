var http = require('http');
var fs = require('fs');

var server = http.createServer();

var SITE_ROOT = '/home/lee/cs100/www';

server.listen(18888); // use the last 4 digits of your ID preceded by a 1

server.on('request', function (req, res) {
	console.log(req.url);
	fs.readFile(SITE_ROOT + req.url, function (err, data) {
		if (err) {
			res.end('File not found!');
		} else {
			res.end(data);
		}
	});
	// res.end(fs.readFileSync(SITE_ROOT + req.url));
});
